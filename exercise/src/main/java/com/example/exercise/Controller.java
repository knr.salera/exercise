package com.example.exercise;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class Controller {

    @Autowired
    public EmployeeRepository repository;

    @Autowired
    public  Service service;

    @GetMapping("/employee")
    public List<Employee> fetchEmployeeList() {
            return service.fetchEmployeeList();
    }

    @GetMapping("/employee/{id}")
    public Employee fetchEmployeeById(@PathVariable("id") Long id){
        return service.fetchEmployeeById(id);
    }

    @PostMapping("/employee")
    public Employee addEmployee(@RequestBody Employee employee){
        return service.addEmployee(employee);
    }

    @PutMapping("/employee")
    public Employee editEmployee(@RequestBody Employee employee) {
        return repository.save(employee);
    }

    @DeleteMapping("/employee/{id}")
    public String deleteEmployee(@PathVariable("id") Long id) {
        service.deleteEmployee(id);
        return "Deleted";
    }






}
