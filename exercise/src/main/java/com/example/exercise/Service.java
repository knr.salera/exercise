package com.example.exercise;

import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Optional;

@org.springframework.stereotype.Service
public class Service {

    @Autowired
    public EmployeeRepository repository;

    public List<Employee> fetchEmployeeList() {
        return repository.findAll();
    }

    public Employee fetchEmployeeById(Long employeeId){
        Optional<Employee> employee = repository.findById(employeeId);
        return employee.get();
    }

    public Employee addEmployee(Employee employee) {
        return repository.save(employee);
    }

    public void deleteEmployee(Long id) {
        repository.deleteById(id);
    }

}
